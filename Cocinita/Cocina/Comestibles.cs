﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cocina
{
    public class Comestibles
    {
        public int cantidad;
        public String nombre;
        bool Transferido;

        public void Transferir()
        {
            Transferido = true;
        }
        public Comestibles(string _nombre, int _cantidad)
        {
            cantidad = _cantidad;
            nombre = _nombre;
        }
    }
}
